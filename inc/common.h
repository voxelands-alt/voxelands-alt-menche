#ifndef _COMMON_H_
#define _COMMON_H_

#ifndef LINUX
#ifdef __linux__
#define LINUX
#endif
#endif

#ifdef LINUX

#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#define _POSIX_C_SOURCE 200112L

#ifdef _XOPEN_SOURCE
#undef _XOPEN_SOURCE
#endif
#define _XOPEN_SOURCE 700

#ifdef _DEFAULT_SOURCE
#undef _DEFAULT_SOURCE
#endif
#define _DEFAULT_SOURCE
#endif

#include <stdlib.h>
#include <stdint.h>

#ifndef _HAVE_ARRAY_TYPE
#define _HAVE_ARRAY_TYPE
typedef struct array_s {
	uint32_t type;
	uint32_t length;
	uint32_t size;
	void *data;
} array_t;
#endif

#ifndef _HAVE_FILE_TYPE
#define _HAVE_FILE_TYPE
typedef struct file_s {
	struct file_s *prev;
	struct file_s *next;
	char* path;
	char* name;
	unsigned char* data;
	int size;
	int len;
	int pos;
	int modified;
} file_t;
#endif

#ifndef _HAVE_COLOUR_TYPE
#define _HAVE_COLOUR_TYPE
typedef struct colour_s {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
} colour_t;
#endif

#ifndef _HAVE_V4_TYPE
#define _HAVE_V4_TYPE
typedef struct v4_s {
	float w;
	float x;
	float y;
	float z;
} __attribute__((packed)) v4_t;
#endif

#ifndef _HAVE_V3_TYPE
#define _HAVE_V3_TYPE
typedef struct v3_s {
	float x;
	float y;
	float z;
} __attribute__((packed)) v3_t;
#endif

#ifndef _HAVE_V2_TYPE
#define _HAVE_V2_TYPE
typedef struct v2_s {
	float x;
	float y;
} __attribute__((packed)) v2_t;
#endif

#ifndef _HAVE_AABOX_TYPE
#define _HAVE_AABOX_TYPE
typedef struct aabox_s {
	v3_t min;
	v3_t max;
} aabox_t;
#endif

#ifndef _HAVE_RECT_TYPE
#define _HAVE_RECT_TYPE
typedef struct rect_s {
	int x;
	int y;
	int w;
	int h;
} rect_t;
#endif

#ifndef _HAVE_RECTF_TYPE
#define _HAVE_RECTF_TYPE
typedef struct rectf_s {
	float x;
	float y;
	float w;
	float h;
} rectf_t;
#endif

#ifndef _HAVE_POS_TYPE
#define _HAVE_POS_TYPE
typedef struct pos_s {
	int16_t x;
	int16_t y;
	int16_t z;
} __attribute__((packed)) pos_t;
#endif

#ifndef _HAVE_QUATERNION_TYPE
#define _HAVE_QUATERNION_TYPE
typedef v4_t quaternion_t;
#endif

#ifndef _HAVE_MATRIX_TYPE
#define _HAVE_MATRIX_TYPE
typedef struct matrix_s {
	float data[16];
} matrix_t;
#endif

#define CN_ERROR	0x01
#define CN_WARN		0x02
#define CN_ACTION	0x03
#define CN_CHAT		0x04
#define CN_INFO		0x05
#define CN_DEBUG	0x06

#define VLSTATE_EXIT	0x00
#define VLSTATE_GET	0x01
#define VLSTATE_MENU	0x02
#define VLSTATE_PAUSE	0x03
#define VLSTATE_PLAY	0x04

/* defined in math.c */
int math_next_pow2(int a);
int math_rand_range(int low, int high);
float math_rand_rangef(float low, float high);
float math_radians_to_degrees(float rads);
float math_degrees_to_radians(float degrees);

/* defined in math_quaternion.c */
quaternion_t *quat_create_quat(float x, float y, float z, float w);
quaternion_t *quat_create_euler(float x, float y, float z);
quaternion_t *quat_create_axis(v3_t *v, float angle);
void quat_init_quat(quaternion_t *q, float x, float y, float z, float w);
void quat_init_euler(quaternion_t *q, float x, float y, float z);
void quat_init_axis(quaternion_t *q, v3_t *v, float angle);
void quat_normalise(quaternion_t *q);
void quat_get_axis(quaternion_t *q, v3_t *v, float *angle);
void quat_multiply_vector(quaternion_t *q, quaternion_t *rq, v3_t *v);
void quat_multiply(quaternion_t *q1, quaternion_t *q2, quaternion_t *rq);
void quat_print(quaternion_t *q);
void quat_computew(quaternion_t *q);
void quat_rotate(quaternion_t *q, v3_t *in, v3_t *out);

/* defined in math_vector.c */
void vect_create(v3_t *start, v3_t *end, v3_t *v);
float vect_length(v3_t *v);
void vect_normalise(v3_t *v);
float vect_scalarproduct(v3_t *v1, v3_t *v2);
void vect_crossproduct(v3_t *v1, v3_t *v2, v3_t *v3);
void vect_dotproduct(v3_t *v1, v3_t *v2, v3_t *v3);
void vect_subtract(v3_t *v1, v3_t *v2, v3_t *v3);
float vect_diameter(v3_t *v);
float math_dotproduct(v3_t *v1, v3_t *v2);
float math_distance(v3_t *v1, v3_t *v2);

/* defined in math_matrix.c */
void matrix_init(matrix_t *m);
matrix_t *matrix_create(void);
void matrix_multiply(matrix_t *m, matrix_t *mul);
void matrix_translate(matrix_t *m, float x, float y, float z);
void matrix_translate_v(matrix_t *m, v3_t *v);
void matrix_scale(matrix_t *m, float x, float y, float z);
void matrix_scale_v(matrix_t *m, v3_t *v);
void matrix_rotate(matrix_t *m, float rads, float x, float y, float z);
void matrix_rotate_v(matrix_t *m, float rads, v3_t *v);
void matrix_rotate_x(matrix_t *m, float rads);
void matrix_rotate_y(matrix_t *m, float rads);
void matrix_rotate_z(matrix_t *m, float rads);
void matrix_rotate_deg(matrix_t *m, float degrees, float x, float y, float z);
void matrix_rotate_deg_v(matrix_t *m, float degrees, v3_t *v);
void matrix_rotate_deg_x(matrix_t *m, float degrees);
void matrix_rotate_deg_y(matrix_t *m, float degrees);
void matrix_rotate_deg_z(matrix_t *m, float degrees);
void matrix_rotate_quat(matrix_t *m, quaternion_t *q);

/* defined in string.c */
char* trim(char* str);
char* strdup(const char* str);
int str_sanitise(char* dest, int size, char* str);
int strappend(char* dest, int size, char* str);
int parse_bool(char* str);

/* defined in config.c */
char* config_get(char* name);
int config_get_int(char* name);
float config_get_float(char* name);
int config_get_bool(char* name);
void config_set(char* name, char* value);
int config_set_command(array_t *args);
void config_set_int(char* name, int value);
void config_set_float(char* name, float value);
void config_set_default(char* name, char* value, int (*setter)(char* v));
void config_set_default_int(char* name, int value, int (*setter)(char* v));
void config_set_default_float(char* name, float value, int (*setter)(char* v));
void config_load(char* file);
int config_load_command(array_t *args);
int config_ignore_command(array_t *args);
void config_init(int argc, char** argv);
void config_save(char* section, char* type, char* file);

/* defined in config_default.c */
void config_default_init(void);

/* defined in log.c */
int log_minlevel_setter(char* v);
int log_maxlevel_setter(char* v);
int log_sminlevel_setter(char* v);
int log_smaxlevel_setter(char* v);
int log_cminlevel_setter(char* v);
int log_cmaxlevel_setter(char* v);
int log_file_setter(char* v);
void vlprintf(uint8_t type, char* fmt,...);

/* defined in utf8.c */
int utf8_seqlen(char* str);
uint32_t utf8_nextchar(char* str, int *i);
uint32_t utf8_toutf32(char* src, int size);
int utf8_fromutf32(char *dest, int sz, uint32_t ch);
uint32_t utf16_toutf32(uint16_t *str);
int utf8_offset(char* str, int i);
int utf8_charindex(char* str, int o);
int utf8_strlen(char* str);
void utf8_inc(char* str, int *i);
void utf8_dec(char* str, int *i);
char* utf8_strchr(char* str, uint32_t ch, int *charn);
char* utf8_memchr(char* str, uint32_t ch, size_t sz, int *charn);

/* defined in sys_console.c */
void sys_console_print(char* str, int newline);
void sys_console_printf(char* fmt, ...);
void sys_console_init(void);
void sys_console_exit(void);

/* defined in intl.c */
char* gettext(const char *s);
char* ngettext(const char* s1, const char* s2, int n);
void intl_init(void);

/* defined in time.c */
void time_init(void);
uint32_t time_ticks(void);
void delay(uint32_t ms);
float time_dtime(uint32_t last);
uint32_t interval_delay(uint32_t last, uint32_t hz);
uint32_t calc_fps(uint32_t prev, uint32_t current);

/* defined in command.c */
int command_init(void);
int command_add(char* name, int (*func)(array_t *args));
int command_apply(char* name, char* value);
int command_exec(char* str);
int command_execf(char* str, ...);
void command_save(file_t *f);

/* defined in main.c */
int client_state(int s);

/* defined in world.c */
int world_init(char* name);
void world_exit(void);

#endif
