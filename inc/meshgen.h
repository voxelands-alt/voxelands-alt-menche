#ifndef _MESHGEN_H_
#define _MESHGEN_H_

#include "content.h"
#include "graphics.h"
#include "map.h"

/* defined in meshgen_utils.c */
uint8_t meshgen_hardface(block_t *b1, block_t *b2, uint8_t dir);
void meshgen_hardfaces(chunk_t *ch, block_t *b, pos_t *p, uint8_t f[6]);
block_t *meshgen_get_block(chunk_t *ch, pos_t *p, uint8_t only_chunk);
mesh_t *meshgen_get_mesh(mapobj_t *o, block_t *b, uint8_t face, uint8_t lod);

/* defined in meshgen_chunk.c */
int meshgen_init(void);
void meshgen_exit(void);
void meshgen_add_chunk(chunk_t *ch, pos_t *p);
void meshgen_free_chunk(chunk_t *ch, pos_t *p);

/* defined in meshgen_block.c */
int meshgen_block(mapobj_t *o, block_t *b, v3_t *p, pos_t *bp);

/* defined in meshgen_cubelike.c */
void meshgen_cubelike(mapobj_t *o, block_t *b, v3_t *p, pos_t *bp, uint8_t lod);

#endif
