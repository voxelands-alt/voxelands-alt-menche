#ifndef _CRYPTO_H_
#define _CRYPTO_H_

#include <stdint.h>

/* defined in crypto.c */
uint32_t hash(char* str);
char* base64_encode(char* str);
char* base64_decode(char* str);

#endif
