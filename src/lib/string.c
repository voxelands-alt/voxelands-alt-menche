/************************************************************************
* string.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* remove whitespace from beginning and end of a string */
char* trim(char* str)
{
	int l;

	while (*str && isspace(*str)) {
		str++;
	}

	l = strlen(str);
	while (--l > -1 && (!str[l] || isspace(str[l]))) {
		str[l] = 0;
	}

	return str;
}

/* allocate memory and copy a string */
char* strdup(const char* str)
{
	char* r;
	int l;

	if (!str)
		return NULL;

	l = strlen(str);
	l++;

	r = malloc(l);
	if (!r)
		return NULL;

	if (!memcpy(r,str,l)) {
		free(r);
		return NULL;
	}

	return r;
}

/* append str to dest, only if there's room for all of it */
int strappend(char* dest, int size, char* str)
{
	int l;

	l = strlen(dest);
	l += strlen(str);

	if (l >= size)
		return 0;

	strcat(dest,str);

	return l;
}

/* makes a string safe for use as a file or directory name */
int str_sanitise(char* dest, int size, char* str)
{
	int o = 0;
	int i = 0;
	int lws = 0;

	for (; o<size; i++) {
		if (!str[i]) {
			dest[o] = 0;
			return o;
		}
		if (isalnum(str[i])) {
			lws = 0;
			dest[o++] = str[i];
		}else if (!lws) {
			dest[o++] = '_';
			lws = 1;
		}
	}

	return -1;
}

/* parse a string into a bool true/false 1/0 */
int parse_bool(char* str)
{
	if (str) {
		if (!strcmp(str,"true"))
			return 1;
		if (!strcmp(str,"yes"))
			return 1;
		if (!strcmp(str,"on"))
			return 1;
		if (!strcmp(str,"enabled"))
			return 1;
		if (!strcmp(str,"active"))
			return 1;
		if (strtol(str,NULL,10))
			return 1;
	}

	return 0;
}
