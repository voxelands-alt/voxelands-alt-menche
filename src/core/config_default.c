/************************************************************************
* config_default.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "path.h"
#include "wm.h"
#include "map.h"

void config_default_init()
{
	config_set_default("log.min_level","error",log_minlevel_setter);
	config_set_default("log.max_level","info",log_maxlevel_setter);
	config_set_default("log.system.min_level","chat",log_sminlevel_setter);
	config_set_default("log.system.max_level","info",log_smaxlevel_setter);
	config_set_default("log.console.min_level","chat",log_cminlevel_setter);
	config_set_default("log.console.max_level","info",log_cmaxlevel_setter);

	config_set_default("path.log",NULL,log_file_setter);
	config_set_default("path.data.custom",NULL,path_custom_setter);
	config_set_default("path.screenshot",NULL,path_screenshot_setter);
	config_set_default("world.path",NULL,path_world_setter);

	config_set_default("wm.width","1024",wm_width_setter);
	config_set_default("wm.height","600",wm_height_setter);
	config_set_default("wm.framecap","30",wm_cap_setter);
	config_set_default("wm.fullscreen","false",wm_fullscreen_setter);
	config_set_default("wm.capture_format","png",NULL);

	config_set_default("gl.anisotropic","true",opengl_anisotropic_setter);
	config_set_default("gl.bilinear","true",opengl_bilinear_setter);
	config_set_default("gl.trilinear","true",opengl_trilinear_setter);
	config_set_default("gl.mipmaps","true",opengl_mipmap_setter);
	config_set_default("gl.particles.enabled","true",opengl_particles_setter);
	config_set_default("gl.particles.max","1000",opengl_particles_max_setter);
	config_set_default("gl.bumpmaps","true",opengl_bumpmap_setter);
	config_set_default("gl.psdf","true",opengl_psdf_setter);
	config_set_default("gl.shadows.passes","1",opengl_shadowpass_setter);
	config_set_default("gl.shadows.resolution","512",opengl_shadowsize_setter);

	config_set_default("ui.font.unifont","false",NULL);
	config_set_default("ui.scale","1.0",ui_scale_setter);
	config_set_default("ui.scale.auto","true",ui_autoscale_setter);

	config_set_default("mapgen.queue.max","100",mapgen_queue_max_setter);
	config_set_default("map.seed.random","true",NULL);

/*
	some rtg things that will probably get used here, of sorts
	cmd_add_setter(RTG_SETTER_LITERAL,"wm.distance",wm_distance_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.enabled",sound_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.effects.volume",sound_effects_setter);
	cmd_add_setter(RTG_SETTER_LITERAL,"sound.music.volume",sound_music_setter);
*/
}
