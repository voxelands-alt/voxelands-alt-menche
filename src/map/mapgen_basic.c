/************************************************************************
* mapgen_basic.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#define _MAPGEN_LOCAL
#include "map.h"
#include "content_block.h"

/* generate an underground region */
void mapgen_underground(pos_t *p)
{
	pos_t os[8] = {
		{-16,-16,-16},
		{-16,-16,0},
		{-16,0,0},
		{-16,0,-16},
		{0,-16,-16},
		{0,0,-16},
		{0,-16,0},
		{0,0,0}
	};
	pos_t cp;
	chunk_t *ch;
	int i;

	for (i=0; i<8; i++) {
		cp.x = p->x+os[i].x;
		cp.y = p->y+os[i].y;
		cp.z = p->z+os[i].z;

		ch = mapgen_create_chunk(&cp);
		if (!ch)
			continue;

		mapgen_fill_chunk(ch,CONTENT_STONE);
		ch->state &= ~CHUNK_STATE_UNGENERATED;
		map_add_chunk(ch);
	}
}

/* TODO: should be moved to mapgen_space.c */
void mapgen_space(pos_t *p)
{
	/* TODO: ... and not do this */
	mapgen_air(p);
}

/* generate an air region */
void mapgen_air(pos_t *p)
{
	pos_t os[8] = {
		{-16,-16,-16},
		{-16,-16,0},
		{-16,0,0},
		{-16,0,-16},
		{0,-16,-16},
		{0,0,-16},
		{0,-16,0},
		{0,0,0}
	};
	pos_t cp;
	chunk_t *ch;
	int i;

	for (i=0; i<8; i++) {
		cp.x = p->x+os[i].x;
		cp.y = p->y+os[i].y;
		cp.z = p->z+os[i].z;

		ch = mapgen_create_chunk(&cp);
		if (!ch)
			continue;

		mapgen_fill_chunk(ch,CONTENT_AIR);
		ch->state &= ~CHUNK_STATE_UNGENERATED;
		map_add_chunk(ch);
	}
}
