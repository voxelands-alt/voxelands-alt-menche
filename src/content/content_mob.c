/************************************************************************
* content_mob.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"

static void content_mob_defaults(contentfeatures_t *f)
{
	content_defaults(f);
	/* TODO: this */
}

int content_mob_init()
{
	int i;
	contentfeatures_t *f;
	for (i=0; i<CONTENT_ARRAY_SIZE; i++) {
		f = &contentfeatures[CONTENT_INDEX_MOB][i];
		content_mob_defaults(f);
	}

	return 0;
}
