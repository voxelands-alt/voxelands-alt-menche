/************************************************************************
* block_ground.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "content.h"
#include "content_block.h"

int block_ground_init()
{
	content_t id;
	contentfeatures_t *f;

	id = CONTENT_AIR;
	f = content_features(id);
	f->content = id;
	f->description = gettext("Air");
	f->param1_type = CPT_LIGHT;
	f->draw_type = CDT_AIRLIKE;
	f->light_data = CLM_CLEAR;
	f->collision_info = CCD_WALKABLE | CCD_CRUSHED | CCD_REPLACEABLE;
	f->damage.suffocation = 0;
	f->data.block.air_equivalent = 1;
	f->data.block.digging_info = CDE_UNDIGGABLE;

	id = CONTENT_STONE;
	f = content_features(id);
	f->content = id;
	f->description = gettext("Stone");
	f->param1_type = CPT_MINERAL;
	f->draw_type = CDT_CUBELIKE;
	f->light_data = CLM_BLOCKS;
	f->collision_info = CCD_SELECTABLE;
	f->material_type = CMT_STONE;
	f->hardness = 1.0;
	f->data.block.is_ground_content = 1;
	f->data.block.digging_info = CDE_DIGGABLE;
	f->dug_item.content = id;
	f->materials_info = CMI_BFCULL;
	content_meshgen_textures(f,
		CMD_TEXTURE, CT_BASE0, "stone.png",
		CMD_NONE
	);

	id = CONTENT_MUD;
	f = content_features(id);
	f->content = id;
	f->description = gettext("Mud");
	f->param1_type = CPT_NONE;
	f->draw_type = CDT_DIRTLIKE;
	f->light_data = CLM_BLOCKS;
	f->collision_info = CCD_SELECTABLE;
	f->material_type = CMT_DIRT;
	f->hardness = 1.0;
	f->data.block.is_ground_content = 1;
	f->data.block.digging_info = CDE_DIGGABLE;
	f->dug_item.content = id;
/*
	f->extra_dug_item.content = CONTENT_ROCK;
	f->extra_dug_item_rarity = 3;
	f->extra_dug_item_max_level = 1;
*/
/*
	f->data.block.ondig.replace = CONTENT_FARM_DIRT;
	f->data.block.ondig.replace_requires = CONTENT_WATER;
*/
	f->materials_info = CMI_BFCULL;
	content_meshgen_textures(f,
		CMD_TEXTURE, CT_BASE0, "mud.png",
		CMD_TEXTURE, CT_TOP0, "mud.png",
		CMD_TEXTURE, CT_TOP1, "footsteps.png",
		CMD_NONE
	);

	id = CONTENT_GRASS;
	f = content_features(id);
	f->content = id;
	f->description = gettext("Grass");
	f->param1_type = CPT_NONE;
	f->draw_type = CDT_DIRTLIKE;
	f->light_data = CLM_BLOCKS;
	f->collision_info = CCD_SELECTABLE;
	f->material_type = CMT_DIRT;
	f->hardness = 1.0;
	f->data.block.is_ground_content = 1;
	f->data.block.digging_info = CDE_DIGGABLE;
	f->dug_item.content = id;
/*
	f->data.block.ondig.replace = CONTENT_FARM_DIRT;
	f->data.block.ondig.replace_requires = CONTENT_WATER;
*/
	f->materials_info = CMI_ALPHA_BLEND | CMI_BFCULL;
	content_meshgen_textures(f,
		CMD_TEXTURE, CT_TOP0, "grass.png",
		CMD_TEXTURE, CT_TOP1, "junglegrass.png",
		CMD_TEXTURE, CT_TOP2, "snow.png",
		CMD_TEXTURE, CT_TOP3, "footsteps.png",
		CMD_TEXTURE, CT_SIDES0, "mud.png",
		CMD_TEXTURE, CT_SIDES1, "grass_side.png",
		CMD_TEXTURE, CT_SIDES2, "junglegrass_side.png",
		CMD_TEXTURE, CT_SIDES3, "snow_side.png",
		CMD_NONE
	);

	return 0;
}
