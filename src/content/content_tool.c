/************************************************************************
* content_tool.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"

static void content_tool_defaults(contentfeatures_t *f)
{
	content_defaults(f);
	/* TODO: set texture unknown_item.png (unknown_tool.png?) */
	f->data.tool.type = CTT_NONE;
	f->data.tool.drop_count = -1;
	f->data.tool.liquids_pointable = 0;
	f->data.tool.damaging_nodes_diggable = 1;
	f->data.tool.has_punch_effect = 1;
	f->data.tool.has_unlock_effect = 0;
	f->data.tool.has_rotate_effect = 0;
	f->data.tool.has_fire_effect = 0;
	f->data.tool.dig_time = 3.;
	f->data.tool.level = 0;
	f->data.tool.thrown_item = CONTENT_IGNORE;
}

int content_tool_init()
{
	int i;
	contentfeatures_t *f;
	for (i=0; i<CONTENT_ARRAY_SIZE; i++) {
		f = &contentfeatures[CONTENT_INDEX_TOOL][i];
		content_tool_defaults(f);
	}

	return 0;
}
