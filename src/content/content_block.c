/************************************************************************
* content_block.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"
#include "content_block.h"

static void content_block_defaults(contentfeatures_t *f)
{
	int i;
	content_defaults(f);

	/* TODO: set block materials unknown_block.png */
	f->collision_info = CCD_WALKABLE | CCD_SELECTABLE;
	f->data.block.vertex_alpha = 255;
	f->data.block.post_effect_colour.r = 0;
	f->data.block.post_effect_colour.g = 0;
	f->data.block.post_effect_colour.b = 0;
	f->data.block.post_effect_colour.a = 0;
	f->data.block.onact_also_affects.x = 0;
	f->data.block.onact_also_affects.y = 0;
	f->data.block.onact_also_affects.z = 0;
	f->data.block.home = -1;

	for (i=0; i<6; i++) {
		content_facetext(&f->data.block.facetexts[i],CFT_NONE,0.0,0.0,0.0,0.0);
	}

	f->data.block.is_ground_content = 0;
	f->data.block.flammable = 0;
	f->data.block.air_equivalent = 0;
	f->data.block.digging_info = CDE_DIGGABLE;
	f->data.block.metaid = CONTENT_IGNORE;
	f->data.block.alternate.wallmount = CONTENT_IGNORE;
	f->data.block.alternate.floormount = CONTENT_IGNORE;
	f->data.block.alternate.roofmount = CONTENT_IGNORE;
	f->data.block.alternate.special = CONTENT_IGNORE;
	f->data.block.alternate.lockstate = CONTENT_IGNORE;
	f->data.block.alternate.powerstate = CONTENT_IGNORE;
	f->data.block.liquid.source = CONTENT_IGNORE;
	f->data.block.liquid.flowing = CONTENT_IGNORE;
	f->data.block.liquid.viscosity = 0;
	f->data.block.ondrop.block = CONTENT_IGNORE;
	f->data.block.ondrop.alternate = CONTENT_IGNORE;
	f->data.block.energy.type = CET_NONE;
	f->data.block.energy.drop = 1;
	f->data.block.ondig.replace = CONTENT_IGNORE;
	f->data.block.ondig.replace_requires = CONTENT_IGNORE;
	content_item(&f->data.block.ondig.special_drop,CONTENT_IGNORE,CTT_NONE,0);
	f->data.block.onpunch.replace = CONTENT_IGNORE;
	f->data.block.onpunch.borderstone = 1;
	f->data.block.plant.max_height = 1;
	f->data.block.plant.small_drop = CONTENT_IGNORE;
	f->data.block.plant.large_drop = CONTENT_IGNORE;
	f->data.block.plant.large_count = 3;
	f->data.block.plant.large_gives_small = 0;
	f->data.block.plant.trellis_block = CONTENT_IGNORE;
	f->data.block.plant.fertilizer_affects = 0;
}

int content_block_init()
{
	int i;
	contentfeatures_t *f;
	for (i=0; i<CONTENT_ARRAY_SIZE; i++) {
		f = &contentfeatures[CONTENT_INDEX_BLOCK][i];
		content_block_defaults(f);
	}

	block_ground_init();

	return 0;
}
