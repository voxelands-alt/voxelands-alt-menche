/************************************************************************
* content.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"

contentfeatures_t *contentfeatures[16];

/* sets the default base values */
void content_defaults(contentfeatures_t *f)
{
	aabox_t *b;

	f->content = CONTENT_IGNORE;
	f->param1_type = CPT_NONE;
	f->param2_type = CPT_NONE;
	f->material_type = CMT_AIR;
	f->hardness = 1.0;
	f->description = NULL;
	f->draw_type = CDT_AIRLIKE;
	array_init(&f->materials,ARRAY_TYPE_PTR);
	f->materials_info = CMF_NONE;
	f->overlay = NULL;
	array_init(&f->collision_boxes,ARRAY_TYPE_PTR);
	b = content_box(NULL,-0.5,-0.5,-0.5,0.5,0.5,0.5);
	array_push_ptr(&f->collision_boxes,b);
	f->collision_info = CCD_NONE;
	f->light_data = CLM_CLEAR;
	f->light_source = 0;
	content_item(&f->dug_item,CONTENT_IGNORE,0,0);
	content_item(&f->extra_dug_item,CONTENT_IGNORE,0,0);
	f->extra_dug_item_rarity = 2;
	f->extra_dug_item_min_level = 0;
	f->extra_dug_item_max_level = 100;
	content_item(&f->cook_result,CONTENT_IGNORE,0,0);
	f->fuel_time = 0.0;
	content_item(&f->onuse_replace,CONTENT_IGNORE,0,0);
	f->enchanted_item = CONTENT_IGNORE;
	f->sound.access = NULL;
	f->sound.step = NULL;
	f->sound.dig = NULL;
	f->sound.place = NULL;
	f->sound.punch = NULL;
	f->sound.ambient = NULL;
	f->sound.use = NULL;
	f->damage.hard = 0;
	f->damage.suffocation = 20;
	f->damage.temperature = 0;
	f->damage.pressure = 0;
}

/* returns the features struct of a content id */
contentfeatures_t *content_features(content_t id)
{
	uint16_t type;
	uint16_t t_id;

	type = (id&CONTENT_TYPE_MASK);
	t_id = (id&~CONTENT_TYPE_MASK);

	/* maybe, maybe not */
	if (id == CONTENT_IGNORE)
		return NULL;

	if (!contentfeatures[type])
		return NULL;

	return &contentfeatures[type][t_id];
}

/* initialiser for item_t */
item_t *content_item(item_t *i, content_t content, uint8_t param1, uint8_t param2)
{
	if (!i)
		i = malloc(sizeof(item_t));

	i->content = content;
	i->param1 = param1;
	i->param2 = param2;

	return i;
}

/* initialiser for aabox_t */
aabox_t *content_box(aabox_t *b, float min_x, float min_y, float min_z, float max_x, float max_y, float max_z)
{
	if (!b)
		b = malloc(sizeof(aabox_t));

	b->min.x = min_x;
	b->min.y = min_y;
	b->min.z = min_z;

	b->max.x = max_x;
	b->max.y = max_y;
	b->max.z = max_z;

	return b;
}

/* initialiser for facetext_t */
facetext_t *content_facetext(facetext_t *f, uint16_t type, float x, float y, float w, float h)
{
	if (!f)
		f = malloc(sizeof(facetext_t));

	f->type = type;
	f->pos.x = x;
	f->pos.y = y;
	f->pos.w = w;
	f->pos.h = h;

	return f;
}

/* init all content */
int content_init()
{
	int i;

	for (i=0; i<16; i++) {
		contentfeatures[i] = NULL;
	}

	contentfeatures[CONTENT_INDEX_BLOCK] = malloc(sizeof(contentfeatures_t)*CONTENT_ARRAY_SIZE);
	contentfeatures[CONTENT_INDEX_CLOTHES] = malloc(sizeof(contentfeatures_t)*CONTENT_ARRAY_SIZE);
	contentfeatures[CONTENT_INDEX_MOB] = malloc(sizeof(contentfeatures_t)*CONTENT_ARRAY_SIZE);
	contentfeatures[CONTENT_INDEX_TOOL] = malloc(sizeof(contentfeatures_t)*CONTENT_ARRAY_SIZE);
	contentfeatures[CONTENT_INDEX_CRAFTITEM] = malloc(sizeof(contentfeatures_t)*CONTENT_ARRAY_SIZE);

	content_block_init();
	content_clothes_init();
	content_craftitem_init();
	content_mob_init();
	content_tool_init();

	return 0;
}

/* free all memory */
/* TODO: actually free it all */
void content_exit()
{
	free(contentfeatures[CONTENT_INDEX_BLOCK]);
	free(contentfeatures[CONTENT_INDEX_CLOTHES]);
	free(contentfeatures[CONTENT_INDEX_MOB]);
	free(contentfeatures[CONTENT_INDEX_TOOL]);
	free(contentfeatures[CONTENT_INDEX_CRAFTITEM]);
}
