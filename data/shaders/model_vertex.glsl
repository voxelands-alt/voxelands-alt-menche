#version 330 core

in vec3 position;
in vec3 normals;
in vec2 texcoords;

out vec2 uv;
out vec3 surfaceNormal;

out vec3 toLight;
out vec3 toCam;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main(void) {

	vec3 lightpos = vec3(0.0,10.0,0.0);
	vec4 worldpos = transformationMatrix*vec4(position, 1.0);

	gl_Position = projectionMatrix*viewMatrix*worldpos;

	uv = texcoords;
	surfaceNormal = (transformationMatrix*vec4(normals,0.0)).xyz;
	toLight = lightpos - worldpos.xyz;

	toCam = (inverse(viewMatrix)*vec4(0.0,0.0,0.0,1.0)).xyz - worldpos.xyz;
}
