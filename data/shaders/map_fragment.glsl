#version 330 core

in vec2 uv;

out vec4 out_Colour;

uniform sampler2D texture0;

void main(void)
{
	out_Colour = texture(texture0,uv);
	//out_Colour = vec4(0.0,1.0,0.0,1.0);
}
