#version 330 core

in vec2 position;
in vec2 uvs;

out vec2 uv;

uniform mat4 transformationMatrix;

void main(void) {

	gl_Position = transformationMatrix*vec4(position, 0.0, 1.0);

	uv = uvs;
}
