#version 330 core

in vec3 position;
in vec3 normals;
in vec2 texcoords;

out vec2 uv;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main(void)
{
	vec4 worldpos = transformationMatrix*vec4(position, 1.0);

	gl_Position = projectionMatrix*viewMatrix*worldpos;

	uv = texcoords;
}
